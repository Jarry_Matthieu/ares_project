import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import bd from '../../donnee/bd.json';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(
    private route: ActivatedRoute
  ) { }

  userLastName = bd.user[Number(this.route.snapshot.paramMap.get('id'))].info.lastname;

  ngOnInit(): void {
  }

}
