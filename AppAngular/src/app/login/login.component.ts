import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { Location } from '@angular/common';

import bd from '../../donnee/bd.json';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    email: new FormControl(''),
    pwd: new FormControl(''),
  });
  
  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onSubmit(){
    let userId = 0;
    while (userId<bd.user.length && bd.user[userId].info.email != this.loginForm.value.email) {
      userId++;
    }
    if (userId < bd.user.length && bd.user[userId].info.pwd == this.loginForm.value.pwd){
      this.router.navigate(['user']);
    }
    else {
      console.warn("Mauvais Email ou mauvais mot de passe");
    }
  }
}
