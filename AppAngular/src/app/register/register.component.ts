import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm = new FormGroup({
    nom: new FormControl(''),
    prenom: new FormControl(''),
    email: new FormControl(''),
    pwd: new FormControl('')
  });

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit() : void{
    console.warn(this.registerForm.value.nom);
    console.warn(this.registerForm.value.prenom);
    console.warn(this.registerForm.value.email);
    console.warn(this.registerForm.value.pwd);
  }

}
